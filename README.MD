# yCrawler

## NB! Python 3.7 required

### Run

#### In docker

```bash
docker build -t ycrawler .
docker run --rm -it ycrawler
```

#### On host

```bash
pip install -r requirements.txt
python run ycrawler.py
```
