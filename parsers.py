from html.parser import HTMLParser


class MainPageParser(HTMLParser):

    def __init__(self, html):
        super().__init__()
        self.title_extracting = False
        self._item = {}
        self.items = []
        self.feed(html)

    def handle_starttag(self, tag, attrs):
        if tag not in {'tr', 'a'}:
            return
        attrs_dict = dict(attrs)
        if 'class' in attrs_dict and attrs_dict['class'] == 'athing':
            self._item = {
                'id': attrs_dict['id']
            }

        if 'class' in attrs_dict and attrs_dict['class'] == 'storylink':
            self._item['source'] = attrs_dict['href']
            self.title_extracting = True

        if 'class' in attrs_dict and attrs_dict['class'] == 'spacer':
            self.items.append(self._item)
            self._item = {}

    def handle_endtag(self, tag):
        if tag != 'a':
            return
        self.title_extracting = False

    def handle_data(self, data):
        if self.title_extracting:
            self._item['title'] = data

    def error(self, message):
        pass


class CommentsParser(HTMLParser):

    def __init__(self, html):
        super().__init__()
        self.links = set()
        self._collecting_comments = False
        self.feed(html)

    def handle_starttag(self, tag, attrs):
        if tag not in {'div', 'a'}:
            return
        attrs_dict = dict(attrs)
        if tag == 'div' and 'class' in attrs_dict and attrs_dict['class'] == 'comment':
            self._collecting_comments = True

        if tag == 'div' and 'class' in attrs_dict and attrs_dict['class'] == 'reply':
            self._collecting_comments = False

        if tag == 'a' and self._collecting_comments:
            self.links.add(attrs_dict['href'])

    def error(self, message):
        pass


if __name__ == '__main__':
    """
    just for test
    """
    import urllib.request
    resp = urllib.request.urlopen('https://news.ycombinator.com')
    p = MainPageParser(resp.read().decode())
    print(p.items)

    resp = urllib.request.urlopen('https://news.ycombinator.com/item?id=18638299')
    p = CommentsParser(resp.read().decode())
    print(p.links)
