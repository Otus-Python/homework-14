import asyncio
import concurrent.futures
import logging
import os
import re

import aiohttp


def get_scraped(path: str) -> set:
    """
    Return set of ids of downloaded items

    :param path:
    :return:
    """
    result = set()
    if os.path.isdir(path):
        for name in os.listdir(path):
            id_, _ = name.split('-', 1)
            result.add(id_)
    return result


def save_page(path: str, content: bytes):
    head, _ = os.path.split(path)
    if not os.path.exists(head):
        os.makedirs(head)
    with open(path, 'wb') as fd:
        fd.write(content)


async def fetch(session: aiohttp.ClientSession,
                url: str,
                timeout: float = None,
                raw: bool = False) -> (int, str or bytes):
    client_timeout = aiohttp.ClientTimeout(total=timeout)
    async with session.get(url, timeout=client_timeout) as resp:
        body = await resp.read() if raw else await resp.text()
        return resp.status, body


async def download_one(session: aiohttp.ClientSession,
                       url: str,
                       path: str,
                       timeout: float,
                       semaphore: asyncio.Semaphore,
                       pool: concurrent.futures.ThreadPoolExecutor):
    async with semaphore:
        try:
            status, body = await fetch(session, url, timeout, raw=True)
        except asyncio.TimeoutError:
            logging.info('Timeout during downloading url: {}'.format(url))
        except aiohttp.client_exceptions.ClientConnectionError:
            logging.warning('Connection error during request to {}'.format(url))
        else:
            if status != 200:
                logging.error('Unable to download page {}, '
                              'response with status {}'.format(url, status))
            else:
                loop = asyncio.get_event_loop()
                await loop.run_in_executor(pool, save_page, path, body)


def get_path(id_: str, url: str,
             folder: str, title: str = None,
             comment: bool = False) -> str:
    normalize_name = re.compile(r"[\W]+")
    if title is None:
        title = url

    prefix, filename = url.lower().rstrip('/').rsplit('/', 1)

    if '.' not in filename:
        filename = normalize_name.sub('-', filename) + '.html'
    filename = '-'.join([normalize_name.sub('-', prefix), filename])
    if comment:
        subfolder = '{}-{}/comments'.format(id_, normalize_name.sub('-', title))[-255:]
    else:
        subfolder = '{}-{}'.format(id_, normalize_name.sub('-', title))[-255:]

    return os.path.join(folder, subfolder, filename)
