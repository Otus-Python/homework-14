FROM python:alpine3.7

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY helpers.py helpers.py
COPY parsers.py parsers.py
COPY ycrawler.py ycrawler.py

ENTRYPOINT ["python", "ycrawler.py"]
