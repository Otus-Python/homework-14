import asyncio
import concurrent.futures
import logging

import aiohttp

from helpers import fetch, get_path, download_one, get_scraped
from parsers import MainPageParser, CommentsParser

crawler_config = {
    'connections': 10,  # downloading concurrency
    'timeout': 3,  # connection timeout
    'update_interval': 60 * 5,  # in seconds
    'threads': 15,  # ThreadPool size
    'folder': './news'  #
}


async def enqueue_news_item_for_downloading(queue, item, folder):
    path = get_path(item['id'], item['source'], folder, item['title'])
    await queue.put({
        'url': item['source'],
        'path': path
    })


async def main_loop(session: aiohttp.ClientSession,
                    timeout: float,
                    update_interval: float,
                    scraped: set,
                    download_queue: asyncio.Queue,
                    fetch_comments_queue: asyncio.Queue,
                    folder):
    url = 'https://news.ycombinator.com'
    async with session:
        while True:
            await asyncio.sleep(0)
            logging.info('Fetching articles list...')
            try:
                status, body = await fetch(session, url, timeout)
            except asyncio.TimeoutError:
                logging.info('Timeout during request for main page')
            except aiohttp.client_exceptions.ClientConnectionError:
                logging.warning('Connection error during request to {}'.format(url))
            else:
                if status != 200:
                    logging.info("Unable to download articles list")
                else:
                    parser = MainPageParser(body)

                    for item in parser.items:
                        if item['id'] in scraped:
                            logging.info('Skip already existed item {}'.format(item['id']))
                            continue
                        if not item['source'].startswith('http'):
                            item['source'] = "{}/item?id={}".format(url, item['id'])
                        await enqueue_news_item_for_downloading(download_queue, item, folder)
                        logging.info("Queued for downloading: Item {} - {}".format(
                            item['id'], item['title']
                        ))
                        scraped.add(item['id'])
                        await fetch_comments_queue.put(item)
            logging.info('Update after {} sec'.format(update_interval))
            await asyncio.sleep(update_interval)


async def download_loop(session: aiohttp.ClientSession,
                        timeout: float,
                        queue: asyncio.Queue,
                        semaphore: asyncio.Semaphore,
                        pool: concurrent.futures.ThreadPoolExecutor):
    while True:
        item = await queue.get()
        _ = asyncio.create_task(
            download_one(session, item['url'], item['path'], timeout, semaphore, pool)
        )


async def comments_loop(session: aiohttp.ClientSession,
                        timeout: float,
                        fetch_comments_queue: asyncio.Queue,
                        download_queue: asyncio.Queue,
                        semaphore: asyncio.Semaphore,
                        folder
                        ):
    while True:
        item = await fetch_comments_queue.get()
        async with semaphore:
            url = 'https://news.ycombinator.com/item?id={}'.format(item['id'])
            try:
                code, body = await fetch(session, url, timeout=timeout)
            except asyncio.TimeoutError:
                logging.info("Timeout during request for {}".format(item['source']))
            except aiohttp.client_exceptions.ClientConnectionError:
                logging.warning('Connection error during request to {}'.format(url))
            else:
                if code != 200:
                    logging.info('Unable to download comments for item {}'.format(item['id']))
                else:
                    parser = CommentsParser(body)
                    for link in parser.links:
                        await download_queue.put({
                            "path": get_path(item['id'], link, folder, item['title'], comment=True),
                            "url": link,
                        })


async def main(config: dict):
    """
    application entry point

    :param config:
    :return:
    """
    download_queue = asyncio.Queue()
    fetch_comments_queue = asyncio.Queue()
    semaphore = asyncio.Semaphore(config['connections'])
    timeout = config['timeout']  # type: float
    update_interval = config['update_interval']
    pool = concurrent.futures.ThreadPoolExecutor(config['threads'])
    connector = aiohttp.TCPConnector(ssl=False)  # preventing ssl error for some sites
    scraped = get_scraped(config['folder'])
    folder = config['folder']
    async with aiohttp.ClientSession(connector=connector) as session:
        await asyncio.gather(
            main_loop(session, timeout, update_interval, scraped, download_queue, fetch_comments_queue, folder),
            download_loop(session, timeout, download_queue, semaphore, pool),
            comments_loop(session, timeout, fetch_comments_queue, download_queue, semaphore, folder)
        )


if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')

    try:
        asyncio.run(main(crawler_config))
    except KeyboardInterrupt:
        logging.info("Cancelling...")
